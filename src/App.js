import React ,{Component}from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './component/Header'
import Contents from './component/Contents'
import Contact from './component/Contact'
import Jumbotron from './component/Jumbotron'
import Footer from './component/Footer'
import Hehe from './component/Hehe'
class App extends Component {
  render (){
    return (
<div>
		<Header/>
		<Contents/>
		<Jumbotron/>
		<Hehe/>
		<hr class="my-4"/>
		<Contact/>
		<Footer/>
</div>
);

  }

}

export default App;
