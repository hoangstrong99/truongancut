import React ,{Component}from 'react';

class Jumbotron extends Component {
  render (){
    return (
 <div>

	<div className="jumbotron">
		<div className="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-10">
			<p>Bấm vào để biết thêm thông tin chi tiết về chúng tôi</p>
		</div>
		<div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-2">
			<a href="#">
				<button type="button" className="btn btn-outline-secondary">Read More</button>
			</a>
		</div>
	</div>
</div>

    );

  }

}

export default Jumbotron;