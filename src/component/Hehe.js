import React ,{Component}from 'react';

class Hehe extends Component {
  render (){
    return (
    	<div>
 <div className="container-fluid padding">
    <div className="row welcome text-center">
        <div className="col-12">
            <h1 className="display-4">Meet our team members</h1>
        </div>
    </div>
</div>
<div className="container-fluid padding">
    <div className="row padding">
        <div className="col-md-4">
            <div className="card">
                <img className="card-img-top" src="https://www.telegraph.co.uk/content/dam/football/2019/01/02/TELEMMGLPICT000184652908_trans_NvBQzQNjv4BqEzw5OF8VsBey80ir11mphrnQhfGMHLlLfMYtA4xb69g.jpeg?imwidth=450" width = 'auto'/>
                <div className="card-body">
                    <h4 className="card-title">Ole Gunnar Solskjær</h4>
                    <p className="card-text">Coach Of Team</p>
                    <a href="#" className="btn btn-outline-info">See profile</a>
                </div>
            </div>
        </div>
        <div className="col-md-4">
            <div className="card">
                <img className="card-img-top" src="https://cdn-04.independent.ie/sport/soccer/article38038153.ece/48ab5/AUTOCROP/w620/ipanews_3e6bcc07-a94c-4acc-b530-9fc49ecae115_1" width = 'auto'/>
                <div className="card-body">
                    <h4 className="card-title">
                        Ashley Young  
                    </h4>
                    <p className="card-text">Captain of the Team</p>
                    <a href="#" className="btn btn-outline-info">See profile</a>
                </div>
            </div>
        </div>
        <div className="col-md-4">
            <div className="card">
                <img className="card-img-top" src="https://i1.wp.com/metro.co.uk/wp-content/uploads/2019/01/GettyImages-1081806606.jpg?quality=90&strip=all&zoom=1&resize=644%2C416&ssl=1"/>
                <div className="card-body">
                    <h4 className="card-title">
                        David Degea
                    </h4>
                    <p className="card-text">Tôn Ngộ Không</p>
                    <a href="#" className="btn btn-outline-info">See profile</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    );

  }

}

export default Hehe;